from fastapi import APIRouter,Depends,HTTPException,status


import models
from database import *
from sqlalchemy.orm import Session
from models import Customer
from JWTtoken import create_access_token
from fastapi.security import OAuth2PasswordRequestForm





router = APIRouter(tags=['Authentication'])


@router.post("/login")
def login(request:OAuth2PasswordRequestForm=Depends(),db:Session = Depends(get_db)):
    user=db.query(models.Customer.Customer).filter(models.Customer.Customer.email == request.username).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail='Invalid Credintials')

    access_token = create_access_token(data={"sub": user.email})
    return {"access_token": access_token, "token_type": "bearer"}

from fastapi import APIRouter,Depends
import models
from models import Customer
import scheamas
from scheamas.Customer import Customer
from database import *
from sqlalchemy.orm import Session

from Oauth2 import get_current_user


router = APIRouter(tags=['CUSTOMER'])



@router.post('/create_customer')
def create_customer(request:scheamas.Customer.Customer,db:Session = Depends(get_db)):
    new_customer = models.Customer.Customer(customer_id=request.customer_id,customer_firstname=request.customer_firstname,customer_lastname=request.customer_lastname,contact_no=request.contact_no,email=request.email,password=request.password)
    db.add(new_customer)
    db.commit()
    db.refresh(new_customer)
    return new_customer



@router.get('/get_all_customer',response_model=list[scheamas.Customer.Customer])
def get_all_customer(db:Session = Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    all_customer = db.query(models.Customer.Customer).all()
    return all_customer


@router.get('/get_customer/{id}',response_model=scheamas.Customer.Customer)
def get_customer(id,db:Session = Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    customer=db.query(models.Customer.Customer).filter(models.Customer.Customer.customer_id == id).first()
    return customer

@router.put('/update_customer/{id}')
def update_customer(id,request:scheamas.Customer.Customer,db:Session=Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    customer = db.query(models.Customer.Customer).filter(models.Customer.Customer.customer_id==id).first()
    if customer:
        customer.customer_id = request.customer_id
        customer.customer_firstname = request.customer_firstname
        customer.customer_lastname = request.customer_lastname
        customer.contact_no = request.contact_no
        customer.email = request.email
        customer.password = request.password
        db.commit()
        db.refresh(customer)
        return customer

    else:
        return 'no record'

@router.delete('/delete_customer/{id}')
def delete_customer(id,db:Session = Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    customer = db.query(models.Customer.Customer).filter(models.Customer.Customer.customer_id == id).first()
    if customer:
        db.delete(customer)
        db.commit()
        return 'successfully deleted'

    else:
        return 'no records'


from fastapi import APIRouter,Depends
import models
from models import Restaurant_menu
import scheamas
from scheamas.Restaurant import ShowResto
from database import *
from sqlalchemy.orm import Session


router = APIRouter(tags=['RESTAURANT'])



@router.post('/add_resaurant_menu')
def create_menu(request:scheamas.Restaurant.ShowResto,db:Session=Depends(get_db)):
    new_menu = models.Restaurant_menu.Resto(id=request.id,menu_name=request.menu_name,menu_price=request.menu_price,menu_qty=1)
    db.add(new_menu)
    db.commit()
    db.refresh(new_menu)
    return new_menu


@router.get('/get_all_menu',response_model=list[scheamas.Restaurant.ShowResto])
def get_all_menu(db:Session = Depends(get_db)):
    all_menu = db.query(models.Restaurant_menu.Resto).all()
    return all_menu

@router.get('/get_one_menu/{id}',response_model=scheamas.Restaurant.ShowResto)
def get_one_menu(id: int,db:Session = Depends(get_db)):
    menu = db.query(models.Restaurant_menu.Resto).filter(models.Restaurant_menu.Resto.id == id).first()
    return menu


@router.put('/update_particular_menu/{id}')
def update_particular_menu(id: int,request:scheamas.Restaurant.ShowResto,db:Session=Depends(get_db)):
    update_menu=db.query(models.Restaurant_menu.Resto).filter(models.Restaurant_menu.Resto.id==id).first()
    if update_menu:
        update_menu.id = request.id
        update_menu.menu_name = request.menu_name
        update_menu.menu_price = request.menu_price
        update_menu.menu_qty = request.menu_qty
        db.commit()
        db.refresh(update_menu)
        return update_menu

    else:
        return "no records found"



@router.delete('/delete_menu/{id}')
def delete_menu(id: int,db:Session = Depends(get_db)):
    menu = db.query(models.Restaurant_menu.Resto).filter(models.Restaurant_menu.Resto.id == id).first()
    if menu:
        db.delete(menu)
        db.commit()
        return menu
    else:
        return 'No records found'




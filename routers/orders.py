from fastapi import APIRouter,Depends
import models
from models import Orders
import scheamas
from scheamas.Orders import Orders,ShowOrder
from database import *
from sqlalchemy.orm import Session
from routers.Booking import get_one_booking_info
from routers.Room import get_particular_room,get_particular_roomtype
from Oauth2 import get_current_user


router = APIRouter(tags=['ORDERS'])


@router.post('/create_order')
def create_order(request:scheamas.Orders.Orders,db:Session=Depends(get_db)):
    Order = models.Orders.Orders(id=request.id,booking_ref=request.booking_ref,menu_orderd=request.menu_orderd,order_date=request.order_date,menu_qty=request.menu_qty)
    db.add(Order)
    db.commit()
    db.refresh(Order)
    return Order


@router.get('/getall_orders',response_model=list[ShowOrder])
def get_all_order(db:Session=Depends(get_db)):
    Order = db.query(models.Orders.Orders).all()

    return Order


@router.get('/getone_orders/{id}', response_model=ShowOrder)
def get_one_order(id:int,db: Session = Depends(get_db)):
    Order = db.query(models.Orders.Orders).filter(models.Orders.Orders.id == id).first()

    return Order


@router.delete('/delete_one_orders/{id}')
def delete_order(id:int,db: Session = Depends(get_db)):
    Order = db.query(models.Orders.Orders).filter(models.Orders.Orders.id == id).first()
    if Order:
        db.delete(Order)
        db.commit()
        return 'Successfully deleted'
    else:
        return 'No records'


@router.put('/update_one_orders/{id}')
def update_order(id:int,request:scheamas.Orders.Orders,db: Session = Depends(get_db)):
    Order = db.query(models.Orders.Orders).filter(models.Orders.Orders.id == id).first()
    if Order:
        Order.id = request.id
        Order.booking_ref = request.booking_ref
        Order.menu_orderd = request.menu_orderd
        Order.order_date = request.order_date
        Order.menu_qty = request.menu_qty
        db.commit()
        db.refresh(Order)

        return 'Successfully updated'

    else:
        return 'NO record'




@router.get('/generate_bill/{id}')
def generate_bill(id:int,db: Session = Depends(get_db)):
    Restarant_bill= 2000
    order=get_one_order(id,db)
    booking_id = order.booking_ref
    id=booking_id
    booking= get_one_booking_info(id,db)
    room_id = booking.room_id_ref
    id=room_id
    room = get_particular_room(id,db)

    room_type_id=room.room_type
    id=room_type_id
    room=get_particular_roomtype(id,db)

    TOTAL_BILL=room.room_price+Restarant_bill

    return TOTAL_BILL



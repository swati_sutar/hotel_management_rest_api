from fastapi import APIRouter,Depends
import models
from models import Room
import scheamas
from scheamas.Room import ShowRoomType,ShowRoom
from database import *
from sqlalchemy.orm import Session
from Oauth2 import get_current_user


router = APIRouter()


@router.post('/create_roomtype',tags=['ROOMTYPE'],response_model=ShowRoomType)
def create_roomtype(request:scheamas.Room.ShowRoomType,db:Session=Depends(get_db)):
    new_roomtype = models.Room.RoomType(id=request.id,Type_name=request.Type_name,Disc=request.Disc,room_price=request.room_price)
    db.add(new_roomtype)
    db.commit()
    db.refresh(new_roomtype)
    return new_roomtype


@router.get('/getall_roomtype',tags=['ROOMTYPE'],response_model=list[ShowRoomType])
def get_roomtype(db:Session=Depends(get_db)):
    all_roomtypes = db.query(models.Room.RoomType).all()
    return all_roomtypes

@router.get('/getone_roomtype/{id}',tags=['ROOMTYPE'],response_model=ShowRoomType)
def get_particular_roomtype(id,db:Session=Depends(get_db)):
    roomtype = db.query(models.Room.RoomType).filter(models.Room.RoomType.id == id).first()
    return roomtype

@router.put('/update_roomtype/{id}',tags=['ROOMTYPE'],response_model=ShowRoomType)
def update_particular_roomtype(id,request:scheamas.Room.ShowRoomType,db:Session = Depends(get_db)):
    updated_roomtype = db.query(models.Room.RoomType).filter(models.Room.RoomType.id == id).first()
    if updated_roomtype:
        updated_roomtype.id = request.id
        updated_roomtype.Type_name = request.Type_name
        updated_roomtype.Disc = request.Disc
        updated_roomtype.room_price = request.room_price
        db.commit()
    else:
        return "NO RECORDS"

    return updated_roomtype


@router.post('/create_room',tags=['ROOM'])
def create_room(request:scheamas.Room.Room,db:Session=Depends(get_db)):
    new_room = models.Room.Room(room_id=request.room_id,is_booked=request.is_booked,room_type=request.room_type,house_keep_id=request.house_keep_id)
    db.add(new_room)
    db.commit()
    db.refresh(new_room)
    return new_room

@router.get('/getall_room',tags=['ROOM'],response_model=list[ShowRoom])
def get_allroom(db:Session=Depends(get_db)):
    all_room = db.query(models.Room.Room).all()
    return all_room


@router.get('/get_particular_room/{id}',tags=['ROOM'],response_model=ShowRoom)
def get_particular_room(id,db:Session=Depends(get_db)):
    room = db.query(models.Room.Room).filter(models.Room.Room.room_id==id).first()
    return room


@router.put('/update_particular/{id}',tags=['ROOM'])
def update_particular_room(id,request:scheamas.Room.Room,db:Session = Depends(get_db)):
    updated_room = db.query(models.Room.Room).filter(models.Room.Room.room_id == id).first()
    if updated_room:
        updated_room.room_id = request.room_id
        updated_room.is_booked = request.is_booked
        updated_room.room_type = request.room_type
        updated_room.house_keep_id = request.house_keep_id

        db.commit()
        db.refresh(updated_room)

    else:
        return 'NO RECORD'

    return updated_room


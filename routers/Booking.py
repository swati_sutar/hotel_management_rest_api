from fastapi import APIRouter,Depends
import models
from models import Booking
import scheamas
from scheamas.Booking import Booking
from database import *
from sqlalchemy.orm import Session
from Oauth2 import get_current_user

from routers.Room import get_particular_room

import datetime

router = APIRouter(tags=['BOOKING'])

@router.post('/create_booking')
def create_booking(request:scheamas.Booking.Booking,db:Session=Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    id=request.room_id_ref
    room=get_particular_room(id,db)
    if request.check_in >= datetime.date.today() and request.check_out > datetime.date.today():
        if room.is_booked==False:
            new_booking=models.Booking.Booking(id=request.id,check_in=request.check_in,check_out=request.check_out,cust_id_ref=request.cust_id_ref,room_id_ref=request.room_id_ref,is_active=request.is_active)
            db.add(new_booking)


            room.is_booked=True

            db.commit()
            db.refresh(new_booking)
            db.refresh(room)
            return new_booking

        else:
            return 'room already booked'


    else:
        return 'valid checkin'



@router.get('/get_all_bookings',response_model=list[scheamas.Booking.ShowBooking])
def get_all_booking_info(db:Session = Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    all_Booking = db.query(models.Booking.Booking).all()
    return all_Booking


@router.get('/get_one_bookin/{id}',response_model=scheamas.Booking.ShowBooking)
def get_one_booking_info(id,db:Session = Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    Booking = db.query(models.Booking.Booking).filter(models.Booking.Booking.id==id).first()
    return Booking


@router.patch('/update_particular/{id}')
def update_particular_booking(id,request:scheamas.Booking.Booking,db:Session = Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    updated_booking = db.query(models.Booking.Booking).filter(models.Booking.Booking.id == id).first()
    if updated_booking:
        updated_booking.id = request.id
        updated_booking.cust_id_ref = request.cust_id_ref
        updated_booking.room_id_ref = request.room_id_ref
        updated_booking.check_in = request.check_in
        updated_booking.check_out = request.check_out
        updated_booking.is_active = request.is_active


        db.commit()
        db.refresh(updated_booking)
        return updated_booking
    else:
        return 'no records found'



@router.delete('/cancel_booking/{id}')
def cancel_booking(id:int,db:Session=Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    cannceled_booking=get_one_booking_info(id,db)
    if cannceled_booking:
        id=cannceled_booking.room_id_ref
        room = get_particular_room(id,db)
        room.is_booked = False
        db.delete(cannceled_booking)

        db.commit()
        db.refresh(room)
        return 'Succefully deleted'



@router.get('/refresh_database')
def refresh_after_each_booking(db:Session=Depends(get_db),get_current_user:scheamas.Customer=Depends(get_current_user)):
    all_bookings = get_all_booking_info(db)
    for booking in all_bookings:
        id = booking.room_id_ref
        particular_room = get_particular_room(id,db)
        if booking.check_out > datetime.date.today():
            booking.is_active = True
            particular_room.is_booked = True
            db.commit()
            db.refresh(booking)
            db.refresh(particular_room)


        else:
            booking.is_active = False
            particular_room.is_booked = False
            db.commit()
            db.refresh(booking)
            db.refresh(particular_room)

    return 'updated'




from fastapi import APIRouter,Depends
import models
from models import Housekeeping
import scheamas
from scheamas.Housekeeping import Housekeeping,ShowHousekeeping
from database import *
from sqlalchemy.orm import Session
from Oauth2 import get_current_user


router = APIRouter(tags=['HOUSEKEEPER'])

@router.post('/add_houskeeper',response_model=ShowHousekeeping)
def add_housekeeper(request:scheamas.Housekeeping.ShowHousekeeping,db:Session=Depends(get_db)):
    HOUSEKEEPER=models.Housekeeping.Housekeeping(id=request.id,first_name=request.first_name,last_name=request.last_name,Contact_no=request.Contact_no)
    db.add(HOUSEKEEPER)
    db.commit()
    db.refresh(HOUSEKEEPER)
    return HOUSEKEEPER

@router.get('/getall_housekeeper',response_model=list[ShowHousekeeping])
def get_allhousekeeper(db:Session=Depends(get_db)):
    all_housekeeper=db.query(models.Housekeeping.Housekeeping).all()
    return all_housekeeper

@router.get('/get_particular_housekeeping/{id}',response_model=ShowHousekeeping)
def get_particular_housekeeping(id,db:Session=Depends(get_db)):
    housekeeper=db.query(models.Housekeeping.Housekeeping).filter(models.Housekeeping.Housekeeping.id==id).first()
    return housekeeper

@router.put('/update_particular_housekeeper/{id}',response_model=ShowHousekeeping)
def update_particular_housekeeper(id,request:scheamas.Housekeeping.ShowHousekeeping,db:Session=Depends(get_db)):
    updated_housekeeper_info=db.query(models.Housekeeping.Housekeeping).filter(models.Housekeeping.Housekeeping.id==id).first()
    if updated_housekeeper_info:
        updated_housekeeper_info.id=request.id
        updated_housekeeper_info.first_name=request.first_name
        updated_housekeeper_info.last_name=request.last_name
        updated_housekeeper_info.Contact_no=request.Contact_no

        db.commit()

    else:
        return 'Updated successfully'

    return updated_housekeeper_info
#=============================================


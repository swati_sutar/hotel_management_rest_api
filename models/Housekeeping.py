from database import *
from sqlalchemy import Column,String,Integer
from sqlalchemy.orm import relationship


class Housekeeping(Base):
    __tablename__='housekeeping'
    id=Column(Integer,primary_key=True,index=True)
    first_name=Column(String(40))
    last_name=Column(String(40))
    Contact_no=Column(Integer)

    hk_ref = relationship('Room', back_populates='house_keep_ref')

from database import *
from sqlalchemy import Column,String,Integer,BigInteger
from sqlalchemy.orm import relationship


class Customer(Base):
    __tablename__='customer_info'
    customer_id=Column(Integer,primary_key=True,index=True)
    customer_firstname= Column(String(40),nullable=False,index=True)
    customer_lastname= Column(String(40),nullable=False,index=True)
    contact_no= Column(BigInteger,unique=True)
    email=Column(String(50),unique=True,nullable=False)
    password=Column(String(40),nullable=False)



    book_ref=relationship('Booking',back_populates='cust_ref')


from database import *
from sqlalchemy import Column,String,Integer,Float


class Resto(Base):
    __tablename__ = 'Resto_info'
    id = Column(Integer,primary_key=True,index=True)
    menu_name = Column(String(40))
    menu_price = Column(Float)
    menu_qty = Column(Integer,default=1)



from database import *
from sqlalchemy import Column,String,Integer,Date,ForeignKey
from sqlalchemy.orm import relationship

class Orders(Base):
    __tablename__ = 'Order_info'
    id = Column(Integer,primary_key=True,index=True)
    booking_ref = Column(Integer,ForeignKey('Booking.id'))
    order_date = Column(Date)
    menu_orderd = Column(String(40))
    menu_qty = Column(Integer)

    Booking_ref_key=relationship('Booking',back_populates='order_ref')




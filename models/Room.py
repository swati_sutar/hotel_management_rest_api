from database import *
from sqlalchemy import Column,String,Integer,Float,Boolean,ForeignKey
from sqlalchemy.orm import relationship


class RoomType(Base):
    __tablename__ = 'RoomType'
    id = Column(Integer,primary_key=True,index=True)
    Type_name = Column(String(30),unique=True,nullable=False)
    Disc = Column(String(40))
    room_price = Column(Float)

    room_ref = relationship('Room',back_populates='roomType_ref')



class Room(Base):
    __tablename__='Room'
    room_id= Column(Integer, primary_key=True, index=True)
    is_booked=Column(Boolean,default=False)
    room_type=Column(Integer,ForeignKey('RoomType.id'))
    house_keep_id=Column(Integer,ForeignKey('housekeeping.id'))

    house_keep_ref=relationship('Housekeeping',back_populates='hk_ref')
    roomType_ref = relationship('RoomType', back_populates='room_ref')
    booking_ref = relationship('Booking', back_populates='rooms_ref')






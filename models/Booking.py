from database import *
from sqlalchemy import Column,Integer,Boolean,Date,ForeignKey
import datetime
from sqlalchemy.orm import relationship

class Booking(Base):
    __tablename__ = 'Booking'
    id = Column(Integer,primary_key=True,index=True)
    check_in = Column(Date,default=datetime.datetime.now())
    check_out = Column(Date,default=datetime.datetime.now())
    cust_id_ref = Column(Integer,ForeignKey('customer_info.customer_id'))
    room_id_ref = Column(Integer,ForeignKey('Room.room_id'))
    is_active = Column(Boolean,default=False)
    #relationship

    cust_ref = relationship('Customer',back_populates='book_ref')
    rooms_ref = relationship('Room', back_populates='booking_ref')
    order_ref = relationship('Orders',back_populates='Booking_ref_key')



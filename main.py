from fastapi import FastAPI
from database import *
import models
from models import Room,Housekeeping,Customer,Restaurant_menu,Booking,Orders

from fastapi.security import OAuth2PasswordBearer
from routers import Room,Housekeeping,Customers,Booking,Restaurant,orders,authentication



app=FastAPI(title='HOTEL MANAGEMENT',description='It is not for production')
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

models.Housekeeping.Base.metadata.create_all(engine)
models.Room.Base.metadata.create_all(engine)
models.Customer.Base.metadata.create_all(engine)
models.Restaurant_menu.Base.metadata.create_all(engine)
models.Booking.Base.metadata.create_all(engine)
models.Orders.Base.metadata.create_all(engine)


app.include_router(authentication.router)
app.include_router(Room.router)
app.include_router(Housekeeping.router)
app.include_router(Customers.router)
app.include_router(Booking.router)
app.include_router(Restaurant.router)
app.include_router(orders.router)



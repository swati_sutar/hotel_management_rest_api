from pydantic import BaseModel
import datetime

class Orders(BaseModel):
    id: int
    booking_ref: int
    menu_orderd: str
    order_date: datetime.date
    menu_qty: int

    class Config():
        orm_mode = True




class ShowOrder(BaseModel):
    id: int
    booking_ref: int
    menu_orderd: str
    order_date: datetime.date
    menu_qty: int

    class Config():
        orm_mode = True

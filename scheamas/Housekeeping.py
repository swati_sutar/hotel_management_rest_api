from pydantic import BaseModel


class ShowHousekeeping(BaseModel):
    id:int
    first_name:str
    last_name:str
    Contact_no:int

    class Config():
        orm_mode=True
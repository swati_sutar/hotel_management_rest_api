
from pydantic import BaseModel
from scheamas.Customer import ShowCustomer
from scheamas.Room import ShowRoom
from scheamas.Orders import ShowOrder
import datetime

class Booking(BaseModel):
    id:int
    cust_id_ref:int
    room_id_ref:int
    check_in:datetime.date
    check_out:datetime.date
    is_active:bool



    class Config:
        orm_mode=True


class ShowBooking(BaseModel):
    id: int
    #cust_id_ref: int
    #room_id_ref: int
    check_in: datetime.date
    check_out: datetime.date
    is_active: bool
    cust_ref:ShowCustomer
    rooms_ref:ShowRoom

    #order_ref:ShowOrder

    class Config:
        orm_mode = True
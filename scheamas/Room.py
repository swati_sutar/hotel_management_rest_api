from pydantic import BaseModel
from scheamas.Customer import ShowCustomer
from scheamas.Housekeeping import ShowHousekeeping

class RoomType(BaseModel):

    id:int
    Type_name:str
    Disc:str
    room_price: float
    #room_ref:RoomType
    


class ShowRoomType(BaseModel):

    id:int
    Type_name:str
    Disc:str
    room_price:float
    #room_ref: RoomType

    class Config():
        orm_mode=True


class Room(BaseModel):
    room_id:int
    room_type:int
    is_booked:bool
    house_keep_id:int

    class Config():
        orm_mode=True

class ShowRoom(BaseModel):
    room_id:int
    is_booked: bool
    roomType_ref:ShowRoomType
    house_keep_ref:ShowHousekeeping

    class Config():
        orm_mode=True
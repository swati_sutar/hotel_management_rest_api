from pydantic import BaseModel
class ShowResto(BaseModel):
    id:int
    menu_name:str
    menu_price:float
    menu_qty:int


    class Config:
        orm_mode=True
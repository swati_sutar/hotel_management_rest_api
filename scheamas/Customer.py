from pydantic import BaseModel

class Customer(BaseModel):
    customer_id : int
    customer_firstname : str
    customer_lastname:str
    contact_no:int
    email : str
    password:str

    class Config():
        orm_mode=True


class ShowCustomer(BaseModel):
    customer_id : int
    customer_firstname : str
    customer_lastname:str
    contact_no:int
    email : str


    class Config():
        orm_mode=True



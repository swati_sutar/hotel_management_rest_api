from sqlalchemy import create_engine
SQLALCHEMY_BASE_URI="mysql+pymysql://root:root@localhost/newdb"
engine=create_engine(SQLALCHEMY_BASE_URI,echo=True)

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker



sessionLocal=sessionmaker(bind=engine,autoflush=False,autocommit=False)
Base=declarative_base()

def get_db():
    db=sessionLocal()
    try:
        yield db
    finally:
        db.close()